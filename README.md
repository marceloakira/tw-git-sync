# tw-git-sync

Script to sync a git repository with Tiddlywiki/Nodejs content, therefore each collaborator can push changes to his own branch.

## Requirements:

### GNU/Linux Distibution
* git
```bash
sudo apt install git	
```

* NodeJS:
```bash
sudo apt install nodejs
``` 

* npm
```bash
sudo apt install npm
``` 

* Tiddlywiki - NodeJS Edition
```bash
npm install -g tiddlywiki
```

## Installation

```bash
git clone https://gitlab.com/marceloakira/tw-git-sync
chmod +x tw-git-sync/tw-git-sync
```

## Usage

Before use tw-git-sync, you should have the following information:
* Git Repository with Tiddlywiki content. For example, https://gitlab.com/marceloakira/comp-evol-map
* Your git repository username
* Your git repository password
* A new or existent branch
* If you want to push automatically changes to repository, yes or no.

To execute tw-git-sync:
```bash
cd tw-git-sync
./tw-git-sync
```

tw-git-sync will:
* ask information interactively
* clone the repository
* create or use a existent branch
* automatically push changes back if you want
